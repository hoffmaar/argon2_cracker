#include "GuessCoordinator.h"

#include <CLI/CLI.hpp>

#include <fstream>
#include <memory>
#include <sstream>

static const std::string program_name = "argon2_cracker";

void
buildArguments(CLI::App &app) {

}


int
main(const int argc, const char *const *argv) {
    // Parse arguments
    CLI::App app;
    buildArguments(app);
    CLI11_PARSE(app, argc, argv);

    // Read hashes
    // TODO: Use CLI11
    std::unique_ptr<std::istream> input_stream =
            std::make_unique<std::ifstream>(argv[1]);
    if (!*input_stream) {
        input_stream = std::make_unique<std::istringstream>(argv[1]);
    }

    auto coordinator = GuessCoordinator::fromHashStream(*input_stream);
    // TODO: Use CLI11
    coordinator.guessFromInputStream(std::cin);
}