//
// Created by aron on 13/10/18.
//

#ifndef ARGON2_CRACKER_GUESSER_H
#define ARGON2_CRACKER_GUESSER_H


#include "Hash.h"

#include <argon2-gpu-common/argon2-common.h>
#include <argon2-cuda/globalcontext.h>
#include <argon2-cuda/processingunit.h>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>
#include <variant>

namespace GuesserResult {
    struct None {
    };
    struct NoGuesses {
    };
    struct Some : std::string {
    };

    struct GuesserResult : std::variant<None, NoGuesses, Some> {
    };
}


class Guesser {
public:
    using guess_type = std::shared_ptr<const std::string>;
    using GuesserResult = GuesserResult::GuesserResult;

    const Hash hash;

    Guesser(Hash hash,
            const argon2::cuda::GlobalContext &globalContext,
            const argon2::cuda::Device &device)
            : hash(std::move(hash)),
              globalContext(globalContext),
              device(device) {}

    bool isQueueClosed() const { return queueClosed; }

    void closeQueue(bool b) { queueClosed = b; }

    void addGuess(const guess_type &guess);

    GuesserResult guessOne();

private:
    std::mutex mutex;
    std::deque<guess_type> guess_queue;
    std::condition_variable new_guess;
    std::atomic<bool> queueClosed = false;

    const argon2::cuda::GlobalContext &globalContext;
    const argon2::cuda::Device &device;
    std::optional<argon2::cuda::ProgramContext> programContext;
    std::optional<argon2::cuda::ProcessingUnit> processingUnit;

    std::vector<std::string>
    computeHash(const std::vector<guess_type> &guessBatch);
};

void Guesser::addGuess(const guess_type &guess) {
    std::lock_guard lock{this->mutex};
    if (!this->queueClosed) {
        this->guess_queue.push_back(guess);
        this->new_guess.notify_all();
    }
}

GuesserResult::GuesserResult Guesser::guessOne() {
    using namespace std::chrono_literals;
    using namespace GuesserResult;
    static const size_t BATCH_SIZE = 1024;

    std::vector<guess_type> guessBatch;

    while (true) {
        {
            std::unique_lock lock(this->mutex);
            // sanity check
            if (this->queueClosed) { // this should never happen
                std::cerr << "WTF" << std::endl;
                std::cerr << "Queue gone but still looping" << std::endl;
                std::cerr << "Hash:" << hash.value << std::endl;
                return None;
            }

            // wait for enough guesses to queue up
            while (!this->queueClosed && this->guess_queue.empty()) {
                this->new_guess.wait_for(lock, 1s, [this]() {
                    return this->guess_queue.size() >= BATCH_SIZE
                           || this->queueClosed;
                });
            }

            // No more guesses coming
            if (this->guess_queue.empty() && this->queueClosed) {

            } else if (this->guess_queue.empty()) {
                continue;
            }

            // pop batch from the queue
            guessBatch.assign(
                    this->guess_queue.begin(),
                    this->guess_queue.begin()
                    + std::min(this->guess_queue.size(), BATCH_SIZE)
            );
            this->guess_queue.erase(
                    this->guess_queue.begin(),
                    this->guess_queue.begin()
                    + std::min(this->guess_queue.size(), BATCH_SIZE)
            );
        }

        // do what we're here for
        const std::vector<std::string> results = computeHash(guessBatch);

        // look for match
        const auto password_it = std::find(std::begin(results),
                                           std::end(results),
                                           hash.value);
        if (password_it != std::end(results)) {
            std::lock_guard<std::mutex> lock(this->mutex);
            this->queueClosed = true;
            return *password_it;
        }
    }
    return std::nullopt;
}

std::vector<std::string>
Guesser::computeHash(const std::vector<guess_type> &guessBatch) {
    // make sure that the batch size of the processing unit is appropriate
    if (!this->processingUnit.has_value()
        || this->processingUnit->getBatchSize() != guessBatch.size()) {
        if (!this->programContext.has_value()) {
            // need a ProgramContext first
            this->programContext = argon2::cuda::ProgramContext(
                    &this->globalContext,
                    {this->device},
                    hash.getType(),
                    hash.getVersion()
            );
        }
        this->processingUnit = argon2::cuda::ProcessingUnit(
                &*this->programContext,
                &this->hash.params,
                &this->device,
                guessBatch.size(),
                false,
                true
        );
    }

    // upload passwords to device
    for (size_t i = 0; i < guessBatch.size(); i++) {
        const void *pw = guessBatch[i]->c_str();
        this->processingUnit->setPassword(i, pw, guessBatch[i]->size());
    }

    // do the stuff
    this->processingUnit->beginProcessing();
    this->processingUnit->endProcessing();

    // read bach the produced hashes
    std::vector<std::string> result;
    result.reserve(guessBatch.size());
    for (size_t i = 0; i < guessBatch.size(); i++) {
        const size_t HASH_LENGTH = hash.params.getOutputLength();
        char buffer[HASH_LENGTH];

        this->processingUnit->getHash(i, buffer);

        std::string hash(buffer, HASH_LENGTH);
        result.push_back(hash);
    }

    return result;
}

#endif //ARGON2_CRACKER_GUESSER_H
