//
// Created by aron on 09/10/18.
//

#include "Hash.h"

#include <iostream>
#include <utility>

#include <boost/algorithm/string.hpp>

const auto Hash::hash_regex = // NOLINT(cert-err58-cpp)
        boost::xpressive::sregex::compile(
                "^\\$argon2(?<type>i|d|id)"
                "(?:\\$v=(?<version>16|19))?"
                "\\$m=(?<memory_cost>\\d{1,10}),"
                "t=(?<time_cost>\\d{1,10}),"
                "p=(?<lanes>\\d{1,8})"
                "\\$(?<salt>[A-Za-z0-9+/=]+)"
                "\\$(?<digest>[A-Za-z0-9+/=]+)$"
        );

std::optional<Hash> Hash::fromString(const std::string &value) {
    boost::xpressive::smatch match;
    if (boost::xpressive::regex_match(value, match, hash_regex)) {
        return Hash(value, match);
    } else {
        return std::nullopt;
    }
}

argon2::cuda::ProgramContext Hash::getProgramContext(
        const argon2::cuda::GlobalContext *globalContext,
        const std::vector<argon2::cuda::Device> &devices) const {
    return argon2::cuda::ProgramContext(globalContext, devices,
                                        this->getType(), this->getVersion());
}

Hash::Hash(std::string value, const boost::xpressive::smatch &match)
        : value(std::move(value)),
          regex_match(match),
          params(argon2::Argon2Params(
                  this->getDigestSizeBytes(),
                  this->getSalt().c_str(), this->getSalt().size(),
                  nullptr, 0,
                  nullptr, 0,
                  this->getTimeCost(),
                  this->getMemoryCost(),
                  this->getLaneCount()
          )) {}

argon2::Type Hash::getType() const {
    static const std::map<std::string, argon2::Type> types = {
            {"i",  argon2::Type::ARGON2_I},
            {"d",  argon2::Type::ARGON2_D},
            {"id", argon2::Type::ARGON2_ID}
    };

    return types.at(this->regex_match["type"].str());
}

argon2::Version Hash::getVersion() const {
    static const std::map<std::string, argon2::Version> versions = {
            {"16", argon2::Version::ARGON2_VERSION_10},
            {"19", argon2::Version::ARGON2_VERSION_13}
    };

    const auto capture = this->regex_match["version"];
    if (capture) {
        return versions.at(capture.str());
    } else {
        return argon2::Version::ARGON2_VERSION_10;
    }
}

uint32_t Hash::getMemoryCost() const {
    return static_cast<uint32_t>(
            std::stoul(this->regex_match["memory_cost"].str())
    );
}

uint32_t Hash::getTimeCost() const {
    return static_cast<uint32_t>(
            std::stoul(this->regex_match["time_cost"].str())
    );
}

uint32_t Hash::getLaneCount() const {
    return static_cast<uint32_t>(
            std::stoul(this->regex_match["lanes"].str())
    );
}

std::string Hash::getSalt() const {
    return regex_match["salt"].str();
}

std::string Hash::getDigest() const {
    return regex_match["digest"].str();
}

size_t base64_length(const std::string &base64_in) {
    std::string s(base64_in);
    boost::trim_right_if(s, [](const char c) { return c == '='; });

    size_t result = s.size() / 4 * 3;
    if (s.size() % 4 == 3) {
        result += 2;
    } else if (s.size() % 4 == 2) {
        result += 1;
    }
    return result;
}

size_t Hash::getDigestSizeBytes() const {
    return base64_length(this->getDigest());
}
