//
// Created by aron on 09/10/18.
//

#ifndef ARGON2_CRACKER_HASH_H
#define ARGON2_CRACKER_HASH_H

#include <argon2-gpu-common/argon2params.h>
#include <argon2-cuda/programcontext.h>

#include <optional>
#include <string>

#include <boost/xpressive/xpressive.hpp>


class Hash {
public:
    const std::string value;
    const argon2::Argon2Params params;

    /**
     * Construct a Hash object from a string. This makes it possible to
     * ensuring the hash is formatted properly without throwing exceptions
     * around
     * @param value the proposed Hash as a string
     * @return Constructed Hash if the string matches the proper formatting,
     * @code{std::nullopt} otherwise
     */
    static std::optional<Hash> fromString(const std::string &value);

    Hash(const Hash &other) = default;

    Hash(Hash &&other) = default;

    /**
     * Parse the hash and construct an according ProgramContext instance
     */
    argon2::cuda::ProgramContext getProgramContext(
            const argon2::cuda::GlobalContext *globalContext,
            const std::vector<argon2::cuda::Device> &devices) const;

    argon2::Type getType() const;

    argon2::Version getVersion() const;

    uint32_t getMemoryCost() const;

    uint32_t getTimeCost() const;

    uint32_t getLaneCount() const;

    std::string getSalt() const;

    std::string getDigest() const;

    size_t getDigestSizeBytes() const;

private:
    /**
     * Construct Hash.
     * Private, so that it is only used by fromString()
     * @param value
     * @param match
     */
    Hash(std::string value, const boost::xpressive::smatch &match);

    const boost::xpressive::smatch regex_match;

    static const boost::xpressive::sregex hash_regex;
};


#endif //ARGON2_CRACKER_HASH_H
