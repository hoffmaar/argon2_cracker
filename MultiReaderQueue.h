//
// Created by aron on 10/10/18.
//

#ifndef ARGON2_CRACKER_MULTIREADERQUEUE_H
#define ARGON2_CRACKER_MULTIREADERQUEUE_H

#include <deque>
#include <utility>
#include <shared_mutex>
#include <atomic>

template<class T, class Container = std::deque<T>>
class MultiReaderQueue {
public:
    typedef typename Container::reference reference;
    typedef typename Container::const_reference const_reference;

    class Reader {
        friend MultiReaderQueue<T, Container>;
    public:
        Reader(MultiReaderQueue &q) : queue(q) {}
        Reader(const Reader &other);
        Reader(Reader &&other) = default;
        ~Reader();

        void pop(size_t count = 1);

        reference front();

        const_reference front() const;

        bool empty() const;

        size_t size() const;

    private:
        MultiReaderQueue &queue;
        std::atomic<size_t> advance = 0;
    };

    void push(const T &value);

    void push(T &&value);

    template<class ForwardIt>
    void pushBatch(const ForwardIt &begin, const ForwardIt &end);

    void pop(size_t count = 1);

    template<class... Args>
    decltype(auto) emplace(Args &&... args);

    void swap(MultiReaderQueue<T, Container> &other) noexcept\
    (std::is_nothrow_swappable<Container>::value);

    reference front();

    const_reference front() const;

    reference back();

    const_reference back() const;

    bool empty() const;

    size_t size() const;

private:
    Container container;

    std::vector<const Reader&

    mutable std::shared_mutex mutex;
};

template<class T, class Container>
void MultiReaderQueue<T, Container>::push(const T &value) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    container.push_back(value);
}

template<class T, class Container>
void MultiReaderQueue<T, Container>::push(T &&value) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    container.push_back(std::move(value));
}

template<class T, class Container>
template<class ForwardIt>
void MultiReaderQueue<T, Container>::pushBatch(const ForwardIt &begin,
                                               const ForwardIt &end) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    for (ForwardIt it = begin; it != end; it++) {
        container.push_back(*it);
    }
}

template<class T, class Container>
void MultiReaderQueue<T, Container>::pop(size_t count) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    this->container.erase(std::begin(this->container),
                          std::advance(std::begin(this->container), count));
}

template<class T, class Container>
template<class... Args>
decltype(auto) MultiReaderQueue<T, Container>::emplace(Args &&... args) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    this->container.emplace(std::forward<Args>(args)...);
}

template<class T, class Container>
void
MultiReaderQueue<T, Container>::swap(
        MultiReaderQueue<T, Container> &other) noexcept(\
    std::is_nothrow_swappable<Container>::value) {
    std::unique_lock<std::shared_mutex> lock(this->mutex);
    std::swap(this->container, other.container);
}

template<class T, class Container>
typename MultiReaderQueue<T, Container>::reference
MultiReaderQueue<T, Container>::front() {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.front();
}

template<class T, class Container>
typename MultiReaderQueue<T, Container>::const_reference
MultiReaderQueue<T, Container>::front() const {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.front();
}

template<class T, class Container>
typename MultiReaderQueue<T, Container>::reference
MultiReaderQueue<T, Container>::back() {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.back();
}

template<class T, class Container>
typename MultiReaderQueue<T, Container>::const_reference
MultiReaderQueue<T, Container>::back() const {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.back();
}

template<class T, class Container>
bool MultiReaderQueue<T, Container>::empty() const {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.empty();
}

template<class T, class Container>
size_t MultiReaderQueue<T, Container>::size() const {
    std::shared_lock<std::shared_mutex> lock(this->mutex);
    return this->container.size();
}

template<class T, class Container>
bool operator==(const MultiReaderQueue<T, Container> &lhs,
                const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container == rhs.container;
}

template<class T, class Container>
bool operator!=(const MultiReaderQueue<T, Container> &lhs,
                const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container != rhs.container;
}

template<class T, class Container>
bool operator<(const MultiReaderQueue<T, Container> &lhs,
               const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container < rhs.container;
}

template<class T, class Container>
bool operator<=(const MultiReaderQueue<T, Container> &lhs,
                const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container <= rhs.container;
}

template<class T, class Container>
bool operator>(const MultiReaderQueue<T, Container> &lhs,
               const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container > rhs.container;
}

template<class T, class Container>
bool operator>=(const MultiReaderQueue<T, Container> &lhs,
                const MultiReaderQueue<T, Container> &rhs) {
    std::shared_lock<std::shared_mutex> lock_lhs(lhs.mutex);
    std::shared_lock<std::shared_mutex> lock_rhs(rhs.mutex);
    return lhs.container >= rhs.container;
}

template<class T, class Container, class Alloc>
struct std::uses_allocator<MultiReaderQueue<T, Container>, Alloc> :
        std::uses_allocator<Container, Alloc>::type {
};

#endif //ARGON2_CRACKER_MULTIREADERQUEUE_H
