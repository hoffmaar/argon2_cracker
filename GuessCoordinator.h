//
// Created by aron on 14/10/18.
//

#ifndef ARGON2_CRACKER_GUESSCOORDINATOR_H
#define ARGON2_CRACKER_GUESSCOORDINATOR_H

#include "Guesser.h"

#include <memory>
#include <functional>
#include <thread>
#include <vector>

class GuessCoordinator {
public:
    template<class InputStream>
    void guessFromInputStream(InputStream &inputStream);

    template<class InputIterator>
    GuessCoordinator(InputIterator hashesBegin, InputIterator hashesEnd);

    template<class IStream>
    static GuessCoordinator fromHashStream(IStream &istream);

    void addOnSuccessDelegate(std::function<void(Hash hash,
                                                 std::string answer)> delegate);

private:
    GuessCoordinator() = default;

    std::vector<std::function<void(const Hash &hash,
                                   const std::string &answer)>>
            onSuccessDelegates;

    void callSuccessDelegates(const Hash &hash,
                              const std::string &answer) const;

    argon2::cuda::GlobalContext globalContext;
    std::vector<argon2::cuda::Device> devices;

    std::vector<std::unique_ptr<Guesser>> guessers;

    template<class IStream>
    void readGuesses(IStream &stream);
};

template<class IStream>
void GuessCoordinator::readGuesses(IStream &stream) {
    while (stream.good()) {
        std::string s;
        std::getline(stream, s);

        const auto guess = std::make_shared<const std::string>(s);
        for (auto &guesser : this->guessers) {
            guesser->addGuess(guess);
        }
    }
}

template<class InputStream>
void GuessCoordinator::guessFromInputStream(InputStream &inputStream) {
    std::vector<std::thread> threads;
    threads.reserve(this->guessers.size() + 2);

    threads.emplace_back([this, &inputStream]() {
        this->readGuesses(inputStream);
    });

    for (auto &guesser : this->guessers) {
        threads.emplace_back([&guesser]() {
            guesser->keepGuessing();
        });
    }

    for (auto &t : threads) {
        t.join();
    }
}

template<class InputIterator>
GuessCoordinator::GuessCoordinator(InputIterator hashesBegin,
                                   InputIterator hashesEnd) {
    this->devices = globalContext.getAllDevices();
    if (devices.empty()) {
        std::cerr << "No devices found" << std::endl;
        exit(1);
    }

    for (InputIterator it = hashesBegin; it != hashesEnd; it++) {
        this->guessers.push_back(std::make_unique<Guesser>(*it,
                                                           globalContext,
                                                           devices[0]));
    }

    for (auto &guesser : this->guessers) {
        guesser->addOnSuccessDelegate([this](Hash hash, std::string answer) {

        });
    }
}

void
GuessCoordinator::callSuccessDelegates(const Hash &hash,
                                       const std::string &answer) const {
    for (const auto &delegate : this->onSuccessDelegates) {
        delegate(hash, answer);
    }
}

template<class IStream>
GuessCoordinator GuessCoordinator::fromHashStream(IStream &istream) {
    std::vector<Hash> hashes;
    std::string line;
    while (std::getline(istream, line).good()) {
        if (line.empty()) {
            continue;
        }
        std::optional<Hash> maybeHash = Hash::fromString(line);
        if (maybeHash) {
            hashes.push_back((*maybeHash));
        }
    }

    if (hashes.empty()) {
        std::cerr << "No argon2 hashes found" << std::endl;
        std::exit(1);
    } else {
        std::cerr << "Found " << hashes.size()
                  << ((hashes.size() == 1) ? " hash." : " hashes.")
                  << std::endl;
    }
    return GuessCoordinator(std::begin(hashes), std::end(hashes));
}

void
GuessCoordinator::
addOnSuccessDelegate(std::function<void(Hash hash,
                                        std::string answer)> delegate) {
    this->onSuccessDelegates.push_back(std::move(delegate));
}

#endif //ARGON2_CRACKER_GUESSCOORDINATOR_H
